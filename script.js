// 1. Опишіть своїми словами, що таке метод об'єкту.

//   Метод объекта - это функция, которая является свойством объекта.

// 2. Який тип данних може мати значення властивості об'єкта?

// Ключи свойств могут быть строками или символами. Значения свойств объекта могут быть данными любого типа.

// 3. Об'єкт це посилальний тип данних. Що означає це поняття?

//  Ссылочный тип данных означает, что при копировании объекта копируется не сам объект, а ссылка на него, т.е. собственно копирования исходного объекта не происходит. Из-за этого при изменении свойств в одном из объектов, изменяются свойства и в его копиях.

function createNewUser() {
    const firstName = prompt("Введіть ім'я", "Vasya");
    const lastName = prompt("Введіть прізвище", "Pupkin");

    function getLogin() {
        return (firstName.trim()[0] + lastName.trim()).toLowerCase()
    }

    return {
        firstName: firstName,
        lastName: lastName,
        getLogin: getLogin()
    }
}

const newUser = createNewUser();

console.log(newUser.getLogin);

Object.defineProperties(newUser, {
    firstName: {
        writable: false
    },
    lastName: {
        writable: false
    }
});


